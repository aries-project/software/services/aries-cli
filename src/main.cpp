#include <iostream>
#include <serialxx/thread_safe.hpp>
#include <thread>

#include "shellxx.hpp"

using namespace serialxx;
using namespace shellxx;
using namespace std::chrono_literals;

int main()
{

    Shell shell("shell> ");

    // c.executeCommand("help");

    shell.registerCommand("test",
                          R"(
    Test command.

    >> test <arg1>

    arg1: Mendatory argument #1
    )",
                          [](const Arguments& args) -> int {
                              auto arg1 = getStringArgument(args, 1, "arg1");

                              std::cout << "#1 => " << arg1 << std::endl;

                              return ReturnCode::Ok;
                          });

    int retCode;
    do {
        retCode = shell.readLine();
    } while (retCode != ReturnCode::Quit);

    return 0;
}

// int main()
// {

//     thread_safe::Serial ser("/dev/ttyAMA0", 1000000);

//     ser.setRS485Mode(RS485mode::HARDWARE);
//     ser.setRS485DelayRtsBeforeSend(0);
//     ser.setRS485DelayRtsAfterSend(0);
//     ser.setRtsPolarity(true);

//     try
//     {
//         ser.open();
//     }
//     catch (std::exception &e)
//     {
//         std::cout << "Serial::open error: " << e.what() << std::endl;
//         exit(1);
//     }

//     std::thread rxt([&ser]() {
//         std::cout << "rx: start\n";
//         while (1)
//         {
//             // printf(".\n");
//             try
//             {
//                 char resp[16];
//                 int len = ser.read(resp, 5, 200);
//                 std::cout << "received: " << len << " bytes\n";
//                 for (int i = 0; i < len; i++)
//                 {
//                     // std::cout << "0x" << std::hex << (unsigned char)resp[i] << ",";
//                     printf("0x%02X [%c] ", (unsigned char)resp[i], resp[i]);
//                 }
//                 printf("\n");
//             }
//             catch (std::exception &e)
//             {
//                 // std::cout << e.what();
//                 // break;
//             }

//             std::cout << std::flush;
//         }
//     });

//     std::this_thread::sleep_for(100ms);

//     while (1)
//     {
//         try
//         {
//             // const char data[] = {0x03, 0x00, 0x01, 0x08, 0x47};
//             // const unsigned char data[] = {0x03, 0x00, 0x04, 0xb9, 0x00, 0x01, 0x02, 0x75};
//             const unsigned char data[] = {0x80, 0x00, 0x01, 0x08, 0xc8};
//             // 0300010847

//             // int len = ser.write((const char *)data, sizeof(data) / sizeof(data[0]));

//             // std::cout << "ping (" << len << ")\n";

//             // for (int i = 0; i < (sizeof(data) / sizeof(data[0])); i++)
//             // {
//             //     ser.writeByte(data[i]);
//             //     std::this_thread::sleep_for(1ms);
//             // }

//             // ser.writeByte(0x03);
//             // std::this_thread::sleep_for(1ms);
//             // ser.writeByte(0x00);
//             // std::this_thread::sleep_for(1ms);
//             // ser.writeByte(0x01);
//             // std::this_thread::sleep_for(1ms);
//             // ser.writeByte(0x08);
//             // std::this_thread::sleep_for(1ms);
//             // ser.writeByte(0x47);
//             // std::this_thread::sleep_for(1ms);

//             // std::cout << "sent: " << len << " bytes\n";

//             // char resp[16];
//             // len = ser.read(resp, 5, 500);
//             // std::cout << "received: " << len << " bytes\n";
//             // for (int i = 0; i < len; i++)
//             // {
//             //     // std::cout << "0x" << std::hex << (unsigned char)resp[i] << ",";
//             //     printf("0x%02X, ", (unsigned char)resp[i]);
//             // }

//             // std::cout << std::endl;
//         }
//         catch (std::exception &e)
//         {
//             std::cout << e.what();
//         }

//         std::this_thread::sleep_for(10ms);
//     }

//     rxt.join();

//     return 0;
// }