/**
 * MIT License
 *
 * Copyright (c) 2020 aries-platform / software / libraries
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SHELLXX_HPP
#define SHELLXX_HPP

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <readline/history.h>
#include <readline/readline.h>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

namespace shellxx {

enum ReturnCode
{
    Quit  = -1,
    Ok    = 0,
    Error = 1 // Or greater!
};

using Arguments          = std::vector<std::string>;
using CommandFunction    = std::function<int(const Arguments&)>;
using RegisteredCommands = std::unordered_map<std::string, CommandFunction>;
using CommandsHelpText   = std::unordered_map<std::string, std::string>;

std::string getStringArgument(const Arguments& args,
                              unsigned position,
                              const std::string& name,
                              bool optional                    = false,
                              const std::string& default_value = "")
{
    if (position >= args.size()) {
        if (optional)
            return default_value;

        // Argument is missing
        std::string msg;
        msg = "Argument '" + name + "' is missing.";
        throw std::invalid_argument(msg.c_str());
    }

    return args[position];
}

std::vector<std::string> getStringListArgument(const Arguments& args,
                                               unsigned position,
                                               const std::string& name,
                                               bool optional                                 = false,
                                               const std::vector<std::string>& default_value = {})
{
    std::vector<std::string> result;

    auto value = getStringArgument(args, position, name, optional);
    std::stringstream ss(value);
    while (ss.good()) {
        std::string substr;
        std::getline(ss, substr, ',');
        result.push_back(substr);
    }
    return result;
}

int getIntegerArgument(const Arguments& args,
                       unsigned position,
                       const std::string& name,
                       bool optional     = false,
                       int default_value = 0)
{
    auto value = getStringArgument(args, position, name, optional, std::to_string(default_value));
    return std::stoi(value);
}

int getFloatArgument(const Arguments& args,
                     unsigned position,
                     const std::string& name,
                     bool optional       = false,
                     float default_value = 0.0f)
{
    auto value = getStringArgument(args, position, name, optional, std::to_string(default_value));
    return std::stof(value);
}

int getDoubleArgument(const Arguments& args,
                      unsigned position,
                      const std::string& name,
                      bool optional        = false,
                      double default_value = 0.0)
{
    auto value = getStringArgument(args, position, name, optional, std::to_string(default_value));
    return std::stod(value);
}

class Shell
{
    static Shell* m_current_shell;
    static HISTORY_STATE* m_empty_history;

  public:
    /**
     * @brief Basic constructor.
     */
    explicit Shell(const std::string& prompt = ">", bool add_default_commands = true)
      : m_prompt(prompt)
      , m_error_command_not_found("Command '{0}' not found.")
      , m_info_current_command_from_file("[{0}] {1}")
    {
        rl_attempted_completion_function = &Shell::getCommandCompletions;

        if (add_default_commands) {
            // These are default hardcoded commands.

            // Help command lists available commands.
            m_help["help"]     = R"(
    List available commands.

    >> help <command>

    command: Show help for a specific command.
            )";
            m_commands["help"] = [this](const Arguments& args) {
                auto cmd = getStringArgument(args, 1, "command", true);

                if (!cmd.empty()) {
                    try {
                        auto text = m_help[cmd];
                        if (text.empty()) {
                            std::cout << "No command help.\n";
                        } else {
                            std::cout << m_help[cmd] << std::endl;
                        }

                    } catch (...) {
                        std::string msg = m_error_command_not_found;
                        replaceAll(msg, "{0}", cmd);
                        std::cout << msg << std::endl;
                    }

                    return ReturnCode::Ok;
                }

                auto commands = getRegisteredCommands();
                int i         = 0;
                // int max_cmds  = commands.size();
                bool first   = true;
                bool newline = false;
                for (auto& command : commands) {
                    if (first) {
                        std::cout << "  ";
                        first = false;
                    }

                    std::cout << command.first << " ";
                    newline = false;

                    if (++i == 6) {
                        i = 0;
                        std::cout << std::endl;
                        first   = true;
                        newline = true;
                    }
                }
                if (!newline) {
                    std::cout << std::endl;
                }
                return ReturnCode::Ok;
            };

            // Run command executes all commands in an external file.
            m_help["run"]     = R"(
    Execute commands from a file.

    >> run <filename>

    filename: Path to a command filename.
            )";
            m_commands["run"] = [this](const Arguments& input) {
                if (input.size() < 2) {
                    std::cout << "Usage: " << input[0] << "script_filename\n ";
                    return 1;
                }
                return executeFile(input[1]);
            };
            // Quit and Exit simply terminate the console.
            m_help["quit"]     = R"(
    Quit the shell.

    >> quit
            )";
            m_commands["quit"] = [this](const Arguments&) { return ReturnCode::Quit; };

            m_help["exit"]     = R"(
    Exit the shell.

    >> exit
            )";
            m_commands["exit"] = [this](const Arguments&) { return ReturnCode::Quit; };
        }
    }

    /**
     * @brief Basic destructor.
     */
    ~Shell()
    {
        if (m_history)
            free(m_history);
    }

    /**
     * @brief Sets the prompt for this Shell.
     *
     * @param prompt New prompt message.
     */
    void setPrompt(const std::string& prompt) { m_prompt = prompt; }

    /**
     * @brief Gets the prompt for this Shell
     *
     * @returns Current prompt message.
     */
    std::string getPrompt() const { return m_prompt; }

    /**
     * @brief This function registers a new command within the Shell.
     *
     * If the command already existed, it overwrites the previous entry.
     *
     * @param s The name of the command as inserted by the user.
     * @param f The function that will be called once the user writes the command.
     */
    void registerCommand(const std::string& s, const std::string& help, CommandFunction f)
    {
        m_commands[s] = f;
        m_help[s]     = help;
    }

    /**
     * @brief This function returns a list with the currently available commands.
     *
     * @return A vector containing all registered commands.
     */
    RegisteredCommands& getRegisteredCommands() { return m_commands; }

    /**
     * @brief This function executes an arbitrary string as if it was inserted via stdin.
     *
     * @param command The command that needs to be executed.
     *
     * @return The result of the operation.
     */
    int executeCommand(const std::string& args)
    {
        // Convert input to vector
        Arguments inputs;
        // Group quoted strings as one argument
        std::regex e(R"(([-_.:/,a-zA-Z0-9]+)|(?:\"([^"]+)\"))");
        std::smatch m;
        std::string s = args;

        while (std::regex_search(s, m, e)) {
            std::string argument;
            for (auto& sm : m) {
                // Get the last matched group that is not empty
                if (!sm.str().empty())
                    argument = sm.str();
            }
            inputs.push_back(argument);

            s = m.suffix().str();
        }

        if (inputs.size() == 0)
            return ReturnCode::Ok;

        RegisteredCommands::iterator it;
        if ((it = m_commands.find(inputs[0])) != end(m_commands)) {
            try {
                return static_cast<int>((it->second)(inputs));
            } catch (std::exception& e) {
                std::cout << e.what() << std::endl;
                return ReturnCode::Error;
            }
        }

        std::string msg = m_error_command_not_found;
        replaceAll(msg, "{0}", inputs[0]);
        std::cout << msg << std::endl;
        return ReturnCode::Error;
    }

    /**
     * @brief This function calls an external script and executes all commands inside.
     *
     * This function stops execution as soon as any single command returns something
     * different from 0, be it a quit code or an error code.
     *
     * @param filename The pathname of the script.
     *
     * @return What the last command executed returned.
     */
    int executeFile(const std::string& filename)
    {
        std::ifstream input(filename);
        if (!input) {
            std::cout << "Could not find the specified file to execute.\n";
            return ReturnCode::Error;
        }
        std::string command;
        int counter = 0, result;

        while (std::getline(input, command)) {
            if (command[0] == '#')
                continue; // Ignore comments
            // Report what the Console is executing.
            // std::cout << "[" << counter << "] " << command << '\n';

            std::string msg = m_info_current_command_from_file;
            replaceAll(msg, "{0}", std::to_string(counter));
            replaceAll(msg, "{1}", command);
            std::cout << msg << std::endl;

            if ((result = executeCommand(command)))
                return result;
            ++counter;
            std::cout << '\n';
        }

        // If we arrived successfully at the end, all is ok
        return ReturnCode::Ok;
    }

    /**
     * @brief This function executes a single command from the user via stdin.
     *
     * @return The result of the operation.
     */
    int readLine()
    {
        swapHistory();

        char* buffer = readline(m_prompt.c_str());
        if (!buffer) {
            std::cout << '\n'; // EOF doesn't put last endline so we put that so that it looks uniform.
            return ReturnCode::Quit;
        }

        // TODO: Maybe add commands to history only if succeeded?
        if (buffer[0] != '\0')
            add_history(buffer);

        std::string line(buffer);
        free(buffer);

        return executeCommand(line);
    }

  private:
    std::string m_prompt;
    std::string m_error_command_not_found;
    std::string m_info_current_command_from_file;

    RegisteredCommands m_commands;
    CommandsHelpText m_help;

    HISTORY_STATE* m_history = nullptr;

    Shell(const Shell&) = delete;
    Shell(Shell&&)      = delete;
    Shell& operator=(Shell const&) = delete;
    Shell& operator=(Shell&&) = delete;

    /**
     * @brief Replace all instances of a sub-string with another one.
     *
     * @param s Input string.
     * @param search String to search for.
     * @param replace String to replace with.
     */
    void replaceAll(std::string& s, const std::string& search, const std::string& replace)
    {
        for (size_t pos = 0;; pos += replace.length()) {
            // Locate the substring to replace
            pos = s.find(search, pos);
            if (pos == std::string::npos)
                break;
            // Replace by erasing and inserting
            s.erase(pos, search.length());
            s.insert(pos, replace);
        }
    }

    /**
     * @brief Save current history state.
     */
    void saveState()
    {
        if (m_history)
            free(m_history);
        m_history = history_get_history_state();
    }

    /**
     * @brief Swap history state to current active shell.
     */
    void swapHistory()
    {
        if (Shell::m_current_shell == this)
            return;

        if (!m_history) {
            history_set_history_state(Shell::m_empty_history);
        } else {
            history_set_history_state(m_history);
        }

        Shell::m_current_shell = this;
    }

    static char** getCommandCompletions(const char* text, int start, int)
    {
        using namespace std::placeholders;

        char** completionList = nullptr;

        if (start == 0)
            completionList = rl_completion_matches(text, &Shell::commandIterator);

        return completionList;
    }

    static char* commandIterator(const char* text, int state)
    {
        static RegisteredCommands::iterator it;

        if (!Shell::m_current_shell)
            return nullptr;

        auto& commands = Shell::m_current_shell->getRegisteredCommands();

        if (state == 0)
            it = std::begin(commands);

        while (it != std::end(commands)) {
            auto& command = it->first;
            ++it;
            if (command.find(text) != std::string::npos) {
                return strdup(command.c_str());
            }
        }

        return nullptr;
    }
};

Shell* Shell::m_current_shell         = nullptr;
HISTORY_STATE* Shell::m_empty_history = history_get_history_state();

} // namespace shellxx

#endif /* SHELLXX_HPP */